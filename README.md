# Stick
[Stick](http://plopgrizzly.com/stick) is a Rust library developed by
[Plop Grizzly](http://plopgrizzly.com) for getting joystick, gamepad, or other
controller input.

[Cargo](https://crates.io/crates/stick) /
[Documentation](https://docs.rs/stick) /
[Change Log](http://plopgrizzly.com/stick/changelog.html)

## Features
**stick**'s current features:
* Get controller input
* Remap controller input
* Connect to multiple controllers

**stick**'s planned features:
* Better (faster & simpler) remapping
* Haptic (vibration) support

## Support
**stick**'s current support:
* Linux
* GameCube controllers (with MAYFLASH adapter)
* Flight simulator joystick

**stick**'s planned support:
* Windows
* MacOS
* Android
* XBox controller
* PlayStation controller
* Emulated joystick
* Nintendo switch
* Probably some other controllers

# [Contributing](http://plopgrizzly.com/contributing/en#contributing)