## 0.4
* Removed `Button`, now is incorporated into `Input`.
* Fake Windows support.

## 0.3
* Added `ControllerManager` to simplify API
* Removed `Throttle` struct and `Joystick` struct

## 0.2
* Added remapping
* Use evdev instead of js0

## 0.1
* Initial version - Linux support only